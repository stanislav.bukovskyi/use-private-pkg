package main

import (
	"fmt"
	"net/http"

	"gitlab.com/stanislav.bukovskyi/private-pkg-test/pkg"
)

func handler(w http.ResponseWriter, r *http.Request) {
	pkg.SecretProcess()
	fmt.Fprintf(w, "Secret process is done!")
}

func main() {
	// Register a handler function for the root URL path ("/")
	http.HandleFunc("/", handler)

	// Start the HTTP server on port 8080
	fmt.Println("Server listening on :8080...")
	err := http.ListenAndServe(":9000", nil)
	if err != nil {
		fmt.Println("Error starting the server:", err)
	}
}
