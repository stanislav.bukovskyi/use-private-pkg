# Usage of private package
## For local developement
1. set env var to tell Golang to look for package as private
```bash
export GOPRIVATE=github.com/stanislav.bukovskyi/private-pkg-test
```
2. Providing private module credentials

For HTTPS execute cmd and paste text to [netrc](#netrc) file
```bash
nano ~/.netrc
```
```text
machine gitlab.com
login your_gitlab_username
password your_gitlab_access_token
```

For SSH execute cmd and paste text to [ssh config](#ssh-config) file
```bash
nano ~/.gitconfig
```
```text
[user]
	email = your_gitlab_username@example.com
	name = your_name
	
[url "ssh://git@gitlab.com/"]
	insteadOf = https://gitlab.com/
```
3. Use private package in your project
```bash
go get gitlab.com/stanislav.bukovskyi/private-pkg-test
```
## For CI/CD or Docker
1. Providing private module credentials by GITLAB_TOKEN arg
2. Set git config to use GITLAB_TOKEN
```dockerfile
ARG GITLAB_TOKEN

RUN go env -w GOPRIVATE=gitlab.com/stanislav.bukovskyi/private-pkg-test
RUN git config  --global url."https://oauth2:${GITLAB_TOKEN}@gitlab.com".insteadOf "https://gitlab.com"
```