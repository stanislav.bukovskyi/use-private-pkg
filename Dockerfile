# syntax=docker/dockerfile:1
FROM golang:1.21.4 as builder

ARG GITLAB_TOKEN

RUN go env -w GOPRIVATE=gitlab.com/stanislav.bukovskyi/private-pkg-test
RUN git config  --global url."https://oauth2:${GITLAB_TOKEN}@gitlab.com".insteadOf "https://gitlab.com"

WORKDIR /usr/src/app

COPY go.mod ./
COPY go.sum ./
COPY . .

# -ldflags "-s -w" disables some debug related functionality
RUN CGO_ENABLED=0 go build -ldflags "-s -w" -x -v -o /app ./cmd/main.go

# Pulling image for the service to run on
FROM alpine:3.14

WORKDIR /usr/src/app

COPY --from=builder /app /app

# Set the command to run the application
CMD ["/app"]